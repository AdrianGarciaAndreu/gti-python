import unittest
from libro import *
from autor import *
from examen import *

class Test(unittest.TestCase):

    def test_anyo_numero(self):
        libros = []
        a1 = Autor(1, "Autor 1", "Apellido 1")
        a2 = Autor(2, "Autor 2", "Apellido 2")

        l1= Libro(a1, "Titulo 1", 2000)
        l2= Libro(a1, "Titulo 2", 2005)
        l3= Libro(a2, "Titulo 3", 2010)
        l4= Libro(a1, "Titulo 4", 1994)
        l5= Libro(a2, "Titulo 5", 1963)

        libros.append(l1)
        libros.append(l2)
        libros.append(l3)
        libros.append(l4)
        libros.append(l5)

        self.assertRaisesRegex(
            ValueError, 
            "anyo no valido", 
            mas_antiguos, libros, -1993)
        
    

    def test_lista_no_vacia(self):
        libros = []
       
        self.assertRaisesRegex(
            ValueError, 
            "lista vacia", 
            mas_antiguos, libros, 2000)

    
    def test_autor_valido(self):

        libros = []

        l1= Libro(None, "Titulo 1", 2000)
        self.assertEqual(l1.get_autor(), None)



if __name__ == "__main__":
    unittest.main()

