
from libro import *
from autor import *

def get_list(fichero):

    diccionario = {}

    f = open(fichero, mode="rt", encoding="utf-8")
    lineas_cuenta = 0
    for linea in f:
        lineas_cuenta = lineas_cuenta=+1
        linea = linea[:-1] # elimina el salto de linea del contenido de las palabras
        palabras_linea = linea.split(" ", ) 

        for palabra in palabras_linea:        
            indice = len(palabra)
            if(indice not in diccionario.keys()):
                lista = []
                lista.append(palabra)
                diccionario[indice] = lista
            else:
                lista = diccionario[indice]
                if(palabra not in lista):
                    lista.append(palabra)
                    diccionario.update({indice: lista})
        
    if(lineas_cuenta>0):
        return(diccionario)
    else:
        raise ValueError("El fichero esta vacio")


"""
try:
    print(get_list("texto_ejercicio1.txt"))
except ValueError as err:
    print(err)

"""

def mas_antiguos(libros, anyo):
    if(len(libros)<1): raise ValueError("lista vacia")
    if(anyo>2021 or anyo<1900): raise ValueError("anyo no valido")
    libros_obtenidos = []
    for libro in libros:
        if(int(libro.get_anyo())<=anyo): libros_obtenidos.append(libro.get_titulo())
    return libros_obtenidos


#############################
######## main ###############
#############################


libros = []
a1 = Autor(1, "Autor 1", "Apellido 1")
a2 = Autor(2, "Autor 2", "Apellido 2")

l1= Libro(a1, "Titulo 1", 2000)
l2= Libro(a1, "Titulo 2", 2005)
l3= Libro(a2, "Titulo 3", 2010)
l4= Libro(a1, "Titulo 4", 1994)
l5= Libro(a2, "Titulo 5", 1963)

libros.append(l1)
libros.append(l2)
libros.append(l3)
libros.append(l4)
libros.append(l5)

try:
    print(mas_antiguos(libros, 2005))
except ValueError as err:
    print(err)
